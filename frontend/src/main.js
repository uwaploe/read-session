import 'core-js/stable';
import 'regenerator-runtime/runtime';
import Vue from 'vue';
import App from './App.vue';
import VueSimpleAlert from 'vue-simple-alert';

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.use(VueSimpleAlert);

import * as Wails from '@wailsapp/runtime';

Wails.Init(() => {
	new Vue({
		render: h => h(App)
	}).$mount('#app');
});
