# ABPR Data Export Tool

This repository contains the source code for a simple GUI tool to export data from an ABPR session file. Session files are created by the ABPR Downloader and the ABPR Deployment Monitor.

The application is built using [Wails](https://wails.app).
