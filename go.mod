module rdsession

go 1.16

require (
	bitbucket.org/uwaploe/abprdata v0.9.10
	github.com/sqweek/dialog v0.0.0-20200911184034-8a3d98e8211d
	github.com/wailsapp/wails v1.16.9
	go.etcd.io/bbolt v1.3.5
	google.golang.org/protobuf v1.26.0
)
