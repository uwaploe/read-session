// Rdsession provides a simple GUI to export data from an ABPR session file.
package main

import (
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"github.com/sqweek/dialog"
	"github.com/wailsapp/wails"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/protobuf/proto"
)

var Version = "dev"
var BuildDate = "unknown"

type event struct {
	Name string      `json:"name"`
	Data interface{} `json:"data"`
}

type FileInfo struct {
	Name      string `json:"filename"`
	HasEvents bool   `json:"hasEvents"`
	HasStatus bool   `json:"hasStatus"`
	HasData   bool   `json:"hasData"`
}

type Session struct {
	rt *wails.Runtime
	db *bolt.DB
}

//go:embed frontend/dist/app.js
var js string

//go:embed frontend/dist/app.css
var css string

var ErrNoFile = errors.New("No input file selected")

func main() {

	app := wails.CreateApp(&wails.AppConfig{
		Width:     512,
		Height:    384,
		Title:     fmt.Sprintf("ABPR Session Reader: %s", Version),
		JS:        js,
		CSS:       css,
		Colour:    "#eee",
		Resizable: true,
	})
	app.Bind(&Session{})
	app.Run()
}

func (s *Session) WailsInit(rt *wails.Runtime) error {
	s.rt = rt
	home, err := s.rt.FileSystem.HomeDir()
	if err != nil {
		return err
	}

	dbdir := filepath.Join(home, "data", "ABPR")
	if err := os.Chdir(dbdir); err != nil {
		os.Chdir(home)
	}

	return nil
}

func (s *Session) Load() (*FileInfo, error) {
	path, err := dialog.File().Filter("ABPR session file", "db").Load()
	if err != nil {
		if err == dialog.ErrCancelled {
			err = nil
		}
		return nil, err
	}

	s.db, err = bolt.Open(path, 0644, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, err
	}

	// Determine what information the database contains
	fi := FileInfo{Name: filepath.Base(path)}
	s.db.View(func(tx *bolt.Tx) error {
		fi.HasEvents = tx.Bucket([]byte("events")) != nil
		return nil
	})
	s.db.View(func(tx *bolt.Tx) error {
		fi.HasStatus = tx.Bucket([]byte("deployment-status")) != nil
		return nil
	})
	s.db.View(func(tx *bolt.Tx) error {
		fi.HasData = tx.Bucket([]byte("stored-data")) != nil
		return nil
	})

	return &fi, nil
}

// Id extracts the session ID from the database filename; session_ID.db
func (s *Session) Id() string {
	if s.db == nil {
		return ""
	}

	parts := strings.Split(filepath.Base(s.db.Path()), "_")
	if len(parts) != 2 {
		return ""
	}

	if i := strings.Index(parts[1], "."); i > 0 {
		return parts[1][:i]
	}
	return parts[1]
}

func (s *Session) ExportEvents() (string, error) {
	if s.db == nil {
		return "", ErrNoFile
	}

	path, err := dialog.File().Filter("Event files", "txt").Title("Export events").Save()
	if err != nil {
		return "", err
	}

	f, err := os.Create(path)
	if err != nil {
		return path, err
	}
	defer f.Close()

	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("events"))
		b.ForEach(func(k, v []byte) error {
			rec := event{}
			err := json.Unmarshal(v, &rec)
			if err != nil {
				return err
			}
			fmt.Fprintf(f, "%s [%s] %v\n", k, rec.Name, rec.Data)
			return nil
		})
		return nil
	})

	return path, err
}

func (s *Session) ExportStatus() (string, error) {
	if s.db == nil {
		return "", ErrNoFile
	}

	path, err := dialog.File().Filter("CSV files", "csv").Title("Export deployment status").Save()
	if err != nil {
		return "", err
	}

	f, err := os.Create(path)
	if err != nil {
		return path, err
	}
	defer f.Close()

	fmt.Fprintln(f, "time,pressure,temperature,pitch,roll,speed")
	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("deployment-status"))
		b.ForEach(func(k, v []byte) error {
			rec := &abprdata.Status{}
			err := proto.Unmarshal(v, rec)
			if err != nil {
				return err
			}
			fmt.Fprintln(f, rec.AsCsv())
			return nil
		})
		return nil
	})

	return path, err
}

func (s *Session) ExportData() (string, error) {
	if s.db == nil {
		return "", ErrNoFile
	}

	path, err := dialog.File().Filter("CSV files", "csv").Title("Export ABPR data").Save()
	if err != nil {
		return "", err
	}

	f, err := os.Create(path)
	if err != nil {
		return path, err
	}
	defer f.Close()

	fmt.Fprintln(f, "time,pressure,temperature")
	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("stored-data"))
		b.ForEach(func(k, v []byte) error {
			rec := &abprdata.Record{}
			err := proto.Unmarshal(v, rec)
			if err != nil {
				return err
			}
			fmt.Fprintln(f, rec.AsCsv())
			return nil
		})
		return nil
	})

	return path, err
}
